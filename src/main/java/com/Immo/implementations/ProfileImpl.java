package com.Immo.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Immo.entities.Profil;
import com.Immo.repositories.ProfilRepo;



@RestController
@RequestMapping("/profile")
public class ProfileImpl {
    @Autowired
  	 ProfilRepo  profilRepo;
		 
@GetMapping("/lister")
public List<Profil> GetAll(){
    return profilRepo.findAll();
		   
}

@GetMapping("/details/{id}")
public ResponseEntity<?> details(@PathVariable int idProfil){
	Profil p = profilRepo.findProfilById(idProfil);
	if (p!=null)
		return ResponseEntity.ok(p);
	else
		return ResponseEntity.notFound().build();
}



@PostMapping("/Ajouter")
public ResponseEntity<?> AjouterProfile(Profil p){
	@SuppressWarnings("unused")
	Profil  p1=profilRepo.save(p);
	return details(p.getIdProfil());

           }


@PutMapping("/Modifier")
public ResponseEntity<?> ModifierProdile(@RequestBody Profil q){	
	if (ProfileExists(q.getIdProfil())){
		profilRepo.saveAndFlush(q);
		return ResponseEntity.ok(q);
	         }
	            return ResponseEntity.notFound().build();
								
            }
		  


@DeleteMapping("/supprimer/{id}")
public ResponseEntity<?> SupprimerProfile(@PathVariable int idProfile){
      if (ProfileExists(idProfile))
         {
    	  profilRepo.deleteById(idProfile);
	         return ResponseEntity.ok().build();
                }
        return ResponseEntity.notFound().build();
         }




private boolean ProfileExists(int p){
Optional<Profil> profil = profilRepo.findById(p);
       if (profil!=null)
         return true;
         else
           return false;
              }
	

}
