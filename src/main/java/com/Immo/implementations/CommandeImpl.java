package com.Immo.implementations;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import  com.Immo.entities.*;
import com.Immo.repositories.CommandeRepo;

@RestController
@RequestMapping("/commande")
public class CommandeImpl {
	
	@Autowired
	
	CommandeRepo commandeRepo;
	
	 
@GetMapping("/lister")
public List<Commande>  GetAll(){
   return commandeRepo.findAll();
		   
}

@GetMapping("/details/{id}")
public ResponseEntity<?> details(@PathVariable int IdCommande){
	Commande c = commandeRepo.findByIdCommande(IdCommande);
	if (c!=null)
		return ResponseEntity.ok(c);
	else
		return ResponseEntity.notFound().build();
}

@PostMapping("/Ajouter")
public ResponseEntity<?> AjouterCommande (@RequestBody Commande c){	
 @SuppressWarnings("unused")
Commande com=commandeRepo.save(c);
 return details(c.getIdCommande());
}


@PutMapping("/Modifier")
 public ResponseEntity<?> ModifierCommande(@RequestBody Commande c){	
	if (CommandeExists(c.getIdCommande())){
		commandeRepo.saveAndFlush(c);
		return ResponseEntity.ok(c);
	}
	return ResponseEntity.notFound().build();
								
}  

@DeleteMapping("/supprimer/{id}")
public ResponseEntity<?> SupprimerCommande(@PathVariable int idCommande){
	if (CommandeExists(idCommande))
	{
		commandeRepo.deleteById(idCommande);
		return ResponseEntity.ok().build();
	}
	return ResponseEntity.notFound().build();
}




 private boolean CommandeExists(int com){
    Optional<Commande> commande = commandeRepo.findById(com);
    if (commande!=null)
        return true;
    else
        return false;
}
}
