package com.Immo.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Immo.entities.Client;
import com.Immo.entities.Quartier;
import com.Immo.repositories.ClientRepo;
import com.Immo.repositories.QuartierRepo;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/quartier")
public class QuartierImpl {

	
	
	    @Autowired
	   	 QuartierRepo  quartierRepo;
			 
	@GetMapping("/lister")
	public List<Quartier> GetAll(){
	     return quartierRepo.findAll();
			   
	}
	
	@GetMapping("/details/{id}")
	public ResponseEntity<?> details(@PathVariable int idQuartier){
		Quartier q = quartierRepo.findQuartierById(idQuartier);
		if (q!=null)
			return ResponseEntity.ok(q);
		else
			return ResponseEntity.notFound().build();
	}
	
	
	
	@PostMapping("/Ajouter")
	public ResponseEntity<?> AjouterQuartier(Quartier q){
		Quartier  q1=quartierRepo.save(q);
		return details(q.getIdQuartier());
	
	            }
	
	
	@PutMapping("/Modifier")
	public ResponseEntity<?> ModifierQuartier(@RequestBody Quartier q){	
		if (QuartierExists(q.getIdQuartier())){
			quartierRepo.saveAndFlush(q);
			return ResponseEntity.ok(q);
		         }
		            return ResponseEntity.notFound().build();
									
	             }
			  
	
	
	@DeleteMapping("/supprimer/{id}")
	public ResponseEntity<?> SupprimerQuartier(@PathVariable int idQuartier){
	       if (QuartierExists(idQuartier))
	          {
		          quartierRepo.deleteById(idQuartier);
		         return ResponseEntity.ok().build();
	                 }
	         return ResponseEntity.notFound().build();
	          }
	
	
	
	
	private boolean QuartierExists(int tc){
	Optional<Quartier> quartier = quartierRepo.findById(tc);
	        if (quartier!=null)
	          return true;
	          else
	            return false;
	               }
	}
