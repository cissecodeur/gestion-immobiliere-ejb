package com.Immo.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.Immo.entities.TypeLogements;
import com.Immo.repositories.TypeLogementRepo;


@RestController
@RequestMapping("/typelogement")
public class TypeLogementImpl {

	
	@Autowired	
 private TypeLogementRepo typelogementRepo;
	
	
	@GetMapping("/lister")
	public List<TypeLogements> GetAll(){		
		return typelogementRepo.findAll();	
	}
	
	@GetMapping("/details/{id}")
	public ResponseEntity<?> details(@PathVariable int idTypeLogement){
		TypeLogements tl = typelogementRepo.findByIdTypeLogement(idTypeLogement);
		if (tl!=null)
			return ResponseEntity.ok(tl);
		else
			return ResponseEntity.notFound().build();
	}
	
	@SuppressWarnings("unused")
	@PostMapping("/Ajouter")
	public ResponseEntity<?> AjouterTypeLogement (@RequestBody TypeLogements tl){	
     TypeLogements tl2=typelogementRepo.save(tl);
     return details(tl.getIdTypeLogement());
	}
	
	
	@PutMapping("/Modifier")
	 public ResponseEntity<?> ModifierTypeLogement(@RequestBody TypeLogements tl){	
		if (TypeLogementExists(tl.getIdTypeLogement())){
			typelogementRepo.saveAndFlush(tl);
			return ResponseEntity.ok(tl);
		}
		return ResponseEntity.notFound().build();
									
	}
	

	
	@DeleteMapping("/supprimer/{id}")
    public ResponseEntity<?> SupprimerTypeLogement(@PathVariable int idTypeLogements){
		if (TypeLogementExists(idTypeLogements))
		{
			typelogementRepo.deleteById(idTypeLogements);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
	
	
	
	
	 private boolean TypeLogementExists(int tl){
        Optional<TypeLogements> typeclient = typelogementRepo.findById(tl);
        if (typeclient!=null)
            return true;
        else
            return false;
    }
	
	
	
}
