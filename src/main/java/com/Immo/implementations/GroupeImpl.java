package com.Immo.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Immo.entities.Groupe;
import com.Immo.repositories.GroupeRepo;

@RestController
@RequestMapping("/groupe")
public class GroupeImpl {
	
	
	@Autowired
	GroupeRepo groupeRepo;
	
	@GetMapping("/lister")
	public List<Groupe> GetAll(){
	     return groupeRepo.findAll();
			   
	}
	
	@GetMapping("/details/{id}")
	public ResponseEntity<?> details(@PathVariable int idGroupe){
		Groupe g = groupeRepo.findGroupeById(idGroupe);
		if (g!=null)
			return ResponseEntity.ok(g);
		else
			return ResponseEntity.notFound().build();
	}
	
	
	
	@PostMapping("/Ajouter")
	public ResponseEntity<?> AjouterGroupe(Groupe g){
		@SuppressWarnings("unused")
		Groupe  g1=groupeRepo.save(g);
		return details(g.getIdGroupe());
	
	            }
	
	
	@PutMapping("/Modifier")
	public ResponseEntity<?> ModifierGroupe(@RequestBody Groupe p){	
		if (GroupeExists(p.getIdGroupe())){
			groupeRepo.saveAndFlush(p);
			return ResponseEntity.ok(p);
		         }
		            return ResponseEntity.notFound().build();
									
	             }
			  
	
	
	@DeleteMapping("/supprimer/{id}")
	public ResponseEntity<?> SupprimerGroupe(@PathVariable int idGroupe){
	       if (GroupeExists(idGroupe))
	          {
		          groupeRepo.deleteById(idGroupe);
		         return ResponseEntity.ok().build();
	                 }
	         return ResponseEntity.notFound().build();
	          }
	
	
	
	
	private boolean GroupeExists(int g){
	Optional<Groupe> groupe = groupeRepo.findById(g);
	        if (groupe!=null)
	          return true;
	          else
	            return false;
	               }
	

}
