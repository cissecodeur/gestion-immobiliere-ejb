package com.Immo.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.Immo.entities.Compte;
import com.Immo.repositories.CompteRepo;

@RestController
@RequestMapping("/compte")
public class CompteImpl {


	@Autowired
	CompteRepo compteRepo;
	
	@GetMapping("/lister")
	public List<Compte>  GetAll(){
		return compteRepo.findAll();		
	}

	
	@GetMapping("/details/{id}")
	public ResponseEntity<?> details(@PathVariable int idCompte){
		Compte cp = compteRepo.findCompteById(idCompte);
		if (cp!=null)
			return ResponseEntity.ok(cp);
		else
			return ResponseEntity.notFound().build();
				
	}

	@PostMapping("/ajouter")
	public ResponseEntity<?> AjouterCompte(@RequestBody Compte cp){
		@SuppressWarnings("unused")
		Compte cp1 = compteRepo.save(cp);
		return details (cp.getIdCompte());		
	}
	
	
	@PutMapping("/modifier")
	public ResponseEntity<?> ModifierCompte(@RequestBody Compte cp){
		if (CompteExists(cp.getIdCompte())){
			compteRepo.saveAndFlush(cp);
			return ResponseEntity.ok(cp);
		         }
		            return ResponseEntity.notFound().build();
		
	}
	

	@DeleteMapping("/supprimer/{id}")
	public ResponseEntity<?> SupprimerCompte(@PathVariable int idCompte){
	       if (CompteExists(idCompte))
	          {
		         compteRepo.deleteById(idCompte);
		         return ResponseEntity.ok().build();
	                 }
	         return ResponseEntity.notFound().build();
	          }
	
	

	
	private boolean CompteExists(int co){
	Optional<Compte> compte = compteRepo.findById(co);
	        if (compte!=null)
	          return true;
	          else
	            return false;
	               }
	
	
	
	
}
