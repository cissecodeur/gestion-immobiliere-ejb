package com.Immo.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Immo.entities.Commune;
import com.Immo.repositories.CommuneRepo;

@RestController
@RequestMapping("/commune")
public class CommuneImpl {
	
	
	@Autowired
	CommuneRepo communeRepo;
	
	@GetMapping("/lister")
	public List<Commune>  GetAll(){
		return communeRepo.findAll();		
	}

	
	@GetMapping("/details/{id}")
	public ResponseEntity<?> details(@PathVariable int idCommune){
		Commune co = communeRepo.FindCommuneById(idCommune);
		if (co!=null)
			return ResponseEntity.ok(co);
		else
			return ResponseEntity.notFound().build();
				
	}

	@PostMapping("/ajouter")
	public ResponseEntity<?> AjouterCommune(@RequestBody Commune co){
		@SuppressWarnings("unused")
		Commune co1 = communeRepo.save(co);
		return details (co.getIdCommune());		
	}
	
	
	@PutMapping("/modifier")
	public ResponseEntity<?> ModifierCommune(@RequestBody Commune co){
		if (CommuneExists(co.getIdCommune())){
			communeRepo.saveAndFlush(co);
			return ResponseEntity.ok(co);
		         }
		            return ResponseEntity.notFound().build();
		
	}
	

	@DeleteMapping("/supprimer/{id}")
	public ResponseEntity<?> SupprimerCommune(@PathVariable int idCommune){
	       if (CommuneExists(idCommune))
	          {
		         communeRepo.deleteById(idCommune);
		         return ResponseEntity.ok().build();
	                 }
	         return ResponseEntity.notFound().build();
	          }
	
	

	
	private boolean CommuneExists(int co){
	Optional<Commune> commune = communeRepo.findById(co);
	        if (commune!=null)
	          return true;
	          else
	            return false;
	               }
}
