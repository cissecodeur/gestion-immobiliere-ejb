package com.Immo.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Immo.entities.PieceJointe;
import com.Immo.repositories.PieceJointeRepo;



@RestController
@RequestMapping("/piecejointe")
public class PieceJointeImpl {
	

	@Autowired	
 private PieceJointeRepo piecejointeRepo;
	
	
	@GetMapping("/lister")
	public List<PieceJointe> GetAll(){		
		return piecejointeRepo.findAll();	
	}
	
	@GetMapping("/details/{id}")
	public ResponseEntity<?> details(@PathVariable int idPieceJointe){
		PieceJointe pj = piecejointeRepo.findPieceJointeById(idPieceJointe);
		if (pj!=null)
			return ResponseEntity.ok(pj);
		else
			return ResponseEntity.notFound().build();
	}
	
	@SuppressWarnings("unused")
	@PostMapping("/Ajouter")
	public ResponseEntity<?> AjouterPieceJointe (@RequestBody PieceJointe pj){	
		PieceJointe pj2=piecejointeRepo.save(pj);
     return details(pj.getIdpieceJointes());
	}
	
	
	@PutMapping("/Modifier")
	 public ResponseEntity<?> ModifierPieceJointe(@RequestBody PieceJointe pj){	
		if (PieceJointeExists(pj.getIdpieceJointes())){
			piecejointeRepo.saveAndFlush(pj);
			return ResponseEntity.ok(pj);
		}
		return ResponseEntity.notFound().build();
									
	}
	

	
	@DeleteMapping("/supprimer/{id}")
    public ResponseEntity<?> SupprimerPieceJointe(@PathVariable int idPieceJointe){
		if (PieceJointeExists(idPieceJointe))
		{
			piecejointeRepo.deleteById(idPieceJointe);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
	
	
	
	
	 private boolean PieceJointeExists(int pj){
        Optional<PieceJointe> piecejointe = piecejointeRepo.findById(pj);
        if (piecejointe!=null)
            return true;
        else
            return false;
    }
	
	

}
