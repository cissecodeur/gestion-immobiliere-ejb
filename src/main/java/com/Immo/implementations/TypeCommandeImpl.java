package com.Immo.implementations;

import java.util.List;
import java.util.Optional;

import com.Immo.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Immo.repositories.TypeCommandeRepo;


@RestController
@RequestMapping("/typecommande")
public class TypeCommandeImpl {
	
       @Autowired
	  
	  TypeCommandeRepo typecommandeRepo;
	  
	  
	  @GetMapping("/lister")
	  public List<TypeCommande> getAll(){
	        return typecommandeRepo.findAll();
	  
	  }
	  
	  @GetMapping("/details/{id}")
		public ResponseEntity<?> details(@PathVariable int idTypeCommande){
			TypeCommande tc = typecommandeRepo.findTypeCommandeById(idTypeCommande);
			if (tc!=null)
				return ResponseEntity.ok(tc);
			else
				return ResponseEntity.notFound().build();
		}
	  
		
		@PostMapping("/Ajouter")
		public ResponseEntity<?> AjouterTypeClient (@RequestBody TypeCommande tc){	
	     @SuppressWarnings("unused")
		TypeCommande tc2=typecommandeRepo.save(tc);
	     return details(tc.getIdTypeCommande());
		}
		
	    
	  
		
		@PutMapping("/Modifier")
		 public ResponseEntity<?> ModifierTypeCommande(@RequestBody TypeCommande tc){	
			if (TypeCommandeExists(tc.getIdTypeCommande())){
				typecommandeRepo.saveAndFlush(tc);
				return ResponseEntity.ok(tc);
			}
			return ResponseEntity.notFound().build();
										
		}
		

		
		@DeleteMapping("/supprimer/{id}")
	    public ResponseEntity<?> SupprimerTypeCommande(@PathVariable int idTypeCommande){
			if (TypeCommandeExists(idTypeCommande))
			{
				typecommandeRepo.deleteById(idTypeCommande);
				return ResponseEntity.ok().build();
			}
			return ResponseEntity.notFound().build();
		}
	  

		 private boolean TypeCommandeExists(int tc){
	        Optional<TypeCommande> typecommande = typecommandeRepo.findById(tc);
	        if (typecommande!=null)
	            return true;
	        else
	            return false;
	    }
	  

}
