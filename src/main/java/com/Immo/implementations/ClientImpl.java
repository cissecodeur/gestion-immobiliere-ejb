package com.Immo.implementations;

import java.util.List;
import java.util.Optional;

import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Immo.entities.Client;
import com.Immo.entities.Commande;
import com.Immo.entities.TypeClient;
import com.Immo.repositories.ClientRepo;


@SuppressWarnings("unused")
@RestController
@RequestMapping("/client")
public class ClientImpl {
	
             @Autowired
	     	 ClientRepo  clientRepo;
			 
   @GetMapping("/lister")
   public List<Client> GetAll(){
           return clientRepo.findAll();
			   
}
   
   @GetMapping("/details/{id}")
	public ResponseEntity<?> details(@PathVariable int id){
		Client tc = clientRepo.findByIdClient(id);
		if (tc!=null)
			return ResponseEntity.ok(tc);
		else
			return ResponseEntity.notFound().build();
	}
   

  @PostMapping("/Ajouter")
  public ResponseEntity<?> AjouterClient(Client c){
	  Client cl=clientRepo.save(c);
	  return details(c.getIdClient());

}


	@PutMapping("/Modifier")
	 public ResponseEntity<?> ModifierClient(@RequestBody Client c){	
		if (ClientExists(c.getIdClient())){
 			clientRepo.saveAndFlush(c);
			return ResponseEntity.ok(c);
		}
		return ResponseEntity.notFound().build();
									
	}
			  


@DeleteMapping("/supprimer/{id}")
public ResponseEntity<?> SupprimerClient(@PathVariable int idClient){
	if (ClientExists(idClient))
	{
		clientRepo.deleteById(idClient);
		return ResponseEntity.ok().build();
	}
	return ResponseEntity.notFound().build();
}




 private boolean ClientExists(int tc){
    Optional<Client> client = clientRepo.findById(tc);
    if (client!=null)
        return true;
    else
        return false;
}
}
