package com.Immo.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Immo.entities.Commande;
import com.Immo.entities.Utilisateur;
import com.Immo.repositories.UtilisateurRepo;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/utilisateur")
public class UtilisateurImpl {
	
	@Autowired
	
	UtilisateurRepo utilisateurRepo;

@GetMapping("/lister")
public List<Utilisateur>  GetAll(){
   return utilisateurRepo.findAll();
		   
}

@GetMapping("/details/{id}")
public ResponseEntity<?> details(@PathVariable int IdUilisateur){
	Utilisateur u = utilisateurRepo.findUtilisateurById(IdUilisateur);
	if (u!=null)
		return ResponseEntity.ok(u);
	else
		return ResponseEntity.notFound().build();
}

@PostMapping("/Ajouter")
public ResponseEntity<?> AjouterUtilisateur (@RequestBody Utilisateur u){	

Utilisateur util=utilisateurRepo.save(u);
 return details(u.getIdUtilisateur());
}


@PutMapping("/Modifier")
 public ResponseEntity<?> ModifierUtilisateur(@RequestBody Utilisateur u){	
	if (UtilisateurExists(u.getIdUtilisateur())){
		utilisateurRepo.saveAndFlush(u);
		return ResponseEntity.ok(u);
	}
	return ResponseEntity.notFound().build();
								
}  

@DeleteMapping("/supprimer/{id}")
public ResponseEntity<?> SupprimerUtilisateur(@PathVariable int idUtilisateur){
	if (UtilisateurExists(idUtilisateur))
	{
		utilisateurRepo.deleteById(idUtilisateur);
		return ResponseEntity.ok().build();
	}
	return ResponseEntity.notFound().build();
}




 private boolean UtilisateurExists(int u){
    Optional<Utilisateur> utilisateur = utilisateurRepo.findById(u);
    if (utilisateur!=null)
        return true;
    else
        return false;
}

}
