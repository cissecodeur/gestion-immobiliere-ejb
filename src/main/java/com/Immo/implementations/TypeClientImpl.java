package com.Immo.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Immo.entities.TypeClient;
import com.Immo.repositories.TypeClientRepo;

@RestController
@RequestMapping("/typeclient")
public class TypeClientImpl {
	
	@Autowired	
 private TypeClientRepo typeclientRepo;
	
	
	@GetMapping("/lister")
	public List<TypeClient> GetAll(){		
		return typeclientRepo.findAll();	
	}
	
	@GetMapping("/details/{id}")
	public ResponseEntity<?> details(@PathVariable int id){
		TypeClient tc = typeclientRepo.findByIdTypeClient(id);
		if (tc!=null)
			return ResponseEntity.ok(tc);
		else
			return ResponseEntity.notFound().build();
	}
	
	@SuppressWarnings("unused")
	@PostMapping("/Ajouter")
	public ResponseEntity<?> AjouterTypeClient (@RequestBody TypeClient tc){	
     TypeClient tc2=typeclientRepo.save(tc);
     return details(tc.getIdTypeClient());
	}
	
	
	@PutMapping("/Modifier")
	 public ResponseEntity<?> ModifierTypeClient(@RequestBody TypeClient tc){	
		if (TypeClientExists(tc.getIdTypeClient())){
			typeclientRepo.saveAndFlush(tc);
			return ResponseEntity.ok(tc);
		}
		return ResponseEntity.notFound().build();
									
	}
	

	
	@DeleteMapping("/supprimer/{id}")
    public ResponseEntity<?> SupprimerTypeClient(@PathVariable int idTypeClient){
		if (TypeClientExists(idTypeClient))
		{
			typeclientRepo.deleteById(idTypeClient);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
	
	
	
	
	 private boolean TypeClientExists(int tc){
        Optional<TypeClient> typeclient = typeclientRepo.findById(tc);
        if (typeclient!=null)
            return true;
        else
            return false;
    }
}
