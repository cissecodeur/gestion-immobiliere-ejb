package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.Compte;

public interface CompteRepo extends JpaRepository<Compte, Integer> {

	Compte findCompteById(int idCompte);
}
