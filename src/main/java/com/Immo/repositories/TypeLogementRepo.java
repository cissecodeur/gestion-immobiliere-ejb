package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.TypeLogements;

public interface TypeLogementRepo extends JpaRepository<TypeLogements, Integer> {
              
      TypeLogements findByIdTypeLogement(int idTypeLogement);
}
