package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.Quartier;

public interface QuartierRepo extends JpaRepository<Quartier, Integer> {
     
	Quartier findQuartierById(int idQuartier);
}
