package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.TypeCommande;

public interface TypeCommandeRepo extends JpaRepository<TypeCommande, Integer> {

	     TypeCommande findTypeCommandeById(int IdTypecommande);
}
