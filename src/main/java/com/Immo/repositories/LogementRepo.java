package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.Logements;

public interface LogementRepo extends JpaRepository<Logements, Integer>{

}
