package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.Utilisateur;

public interface UtilisateurRepo extends JpaRepository<Utilisateur, Integer>{

	Utilisateur findUtilisateurById(int idUtilisateur);
}
