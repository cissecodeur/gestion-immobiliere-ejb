package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.PieceJointe;

public interface PieceJointeRepo extends JpaRepository<PieceJointe, Integer>{
	
	PieceJointe findPieceJointeById(int idPieceJointe);

}
