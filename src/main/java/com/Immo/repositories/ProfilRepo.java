package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.Profil;

public interface ProfilRepo extends JpaRepository<Profil, Integer> {

	   Profil findProfilById(int idProfil);
}
