package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.TypeClient;

public interface TypeClientRepo extends JpaRepository<TypeClient, Integer>{

	 TypeClient findByIdTypeClient(int id);
	
}
