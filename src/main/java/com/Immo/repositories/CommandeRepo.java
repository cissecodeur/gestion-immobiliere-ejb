package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.Commande;

public interface CommandeRepo extends JpaRepository<Commande, Integer> {

	Commande  findByIdCommande(int IdCommande);
	
}
