package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.Groupe;

public interface GroupeRepo extends JpaRepository<Groupe, Integer>{

	Groupe findGroupeById(int idGroupe);
}
