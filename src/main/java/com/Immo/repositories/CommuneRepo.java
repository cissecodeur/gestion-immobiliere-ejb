package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.Commune;

public interface CommuneRepo extends JpaRepository<Commune, Integer>{
 
	 Commune FindCommuneById(int idCommune);
}
