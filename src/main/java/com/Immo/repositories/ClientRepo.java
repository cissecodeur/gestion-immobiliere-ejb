package com.Immo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Immo.entities.Client;


public interface ClientRepo extends JpaRepository<Client, Long>{

	
}
