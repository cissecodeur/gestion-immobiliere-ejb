package com.Immo.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
public class Commande implements Serializable {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCommande;
	private Date dateCommande;
	private int etatCommande;

	
	
	@ManyToOne
	@JoinColumn(name="idTypeCommande",referencedColumnName="idTypeCommande")
	private TypeCommande typecommande;



	public Commande() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Commande(int idCommande, Date dateCommande, int etatCommande, TypeCommande typecommande) {
		super();
		this.idCommande = idCommande;
		this.dateCommande = dateCommande;
		this.etatCommande = etatCommande;
		this.typecommande = typecommande;
	}



	public int getIdCommande() {
		return idCommande;
	}



	public void setIdCommande(int idCommande) {
		this.idCommande = idCommande;
	}



	public Date getDateCommande() {
		return dateCommande;
	}



	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}



	public int getEtatCommande() {
		return etatCommande;
	}



	public void setEtatCommande(int etatCommande) {
		this.etatCommande = etatCommande;
	}



	public TypeCommande getTypecommande() {
		return typecommande;
	}



	public void setTypecommande(TypeCommande typecommande) {
		this.typecommande = typecommande;
	}



	@Override
	public String toString() {
		return "Commande [idCommande=" + idCommande + ", dateCommande=" + dateCommande + ", etatCommande="
				+ etatCommande + ", typecommande=" + typecommande + "]";
	}
	
	
	
	
}
