package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SuppressWarnings("serial")
public class Groupe implements Serializable{

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idGroupe;
	private String libelleGroupe;
	
	public Groupe() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Groupe(int idGroupe, String libelleGroupe) {
		super();
		this.idGroupe = idGroupe;
		this.libelleGroupe = libelleGroupe;
	}

	public int getIdGroupe() {
		return idGroupe;
	}

	public void setIdGroupe(int idGroupe) {
		this.idGroupe = idGroupe;
	}

	public String getLibelleGroupe() {
		return libelleGroupe;
	}

	public void setLibelleGroupe(String libelleGroupe) {
		this.libelleGroupe = libelleGroupe;
	}

	@Override
	public String toString() {
		return "Groupe [idGroupe=" + idGroupe + ", libelleGroupe=" + libelleGroupe + "]";
	}
	
	
	
	
}
