package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SuppressWarnings("serial")
public class Commune implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCommune;
	private String nomClient;
	
	public Commune() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Commune(int idCommune, String nomClient) {
		super();
		this.idCommune = idCommune;
		this.nomClient = nomClient;
	}

	public int getIdCommune() {
		return idCommune;
	}

	public void setIdCommune(int idCommune) {
		this.idCommune = idCommune;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	@Override
	public String toString() {
		return "Commune [idCommune=" + idCommune + ", nomClient=" + nomClient + "]";
	}
	
	

}
