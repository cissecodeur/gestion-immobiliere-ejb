package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SuppressWarnings("serial")
public class PieceJointe implements Serializable{
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idpieceJointes;
	private String libellepieceJointes;
	
	
	public PieceJointe() {
		super();
		// TODO Auto-generated constructor stub
	}


	public PieceJointe(int idpieceJointes, String libellepieceJointes) {
		super();
		this.idpieceJointes = idpieceJointes;
		this.libellepieceJointes = libellepieceJointes;
	}


	public int getIdpieceJointes() {
		return idpieceJointes;
	}


	public void setIdpieceJointes(int idpieceJointes) {
		this.idpieceJointes = idpieceJointes;
	}


	public String getLibellepieceJointes() {
		return libellepieceJointes;
	}


	public void setLibellepieceJointes(String libellepieceJointes) {
		this.libellepieceJointes = libellepieceJointes;
	}


	@Override
	public String toString() {
		return "PieceJointe [idpieceJointes=" + idpieceJointes + ", libellepieceJointes=" + libellepieceJointes + "]";
	}
	
	
	
}
