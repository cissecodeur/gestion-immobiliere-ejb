package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SuppressWarnings("serial")
public class Logements implements Serializable{

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int    idLogement;
	private String libelleLogement;
	private String superficieLogement;
	private String addresseLogement;
	
	public Logements() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Logements(int idLogement, String libelleLogement, String superficieLogement, String addresseLogement) {
		super();
		this.idLogement = idLogement;
		this.libelleLogement = libelleLogement;
		this.superficieLogement = superficieLogement;
		this.addresseLogement = addresseLogement;
	}

	public int getIdLogement() {
		return idLogement;
	}

	public void setIdLogement(int idLogement) {
		this.idLogement = idLogement;
	}

	public String getLibelleLogement() {
		return libelleLogement;
	}

	public void setLibelleLogement(String libelleLogement) {
		this.libelleLogement = libelleLogement;
	}

	public String getSuperficieLogement() {
		return superficieLogement;
	}

	public void setSuperficieLogement(String superficieLogement) {
		this.superficieLogement = superficieLogement;
	}

	public String getAddresseLogement() {
		return addresseLogement;
	}

	public void setAddresseLogement(String addresseLogement) {
		this.addresseLogement = addresseLogement;
	}

	@Override
	public String toString() {
		return "Logements [idLogement=" + idLogement + ", libelleLogement=" + libelleLogement + ", superficieLogement="
				+ superficieLogement + ", addresseLogement=" + addresseLogement + "]";
	}
	
	
	

}
