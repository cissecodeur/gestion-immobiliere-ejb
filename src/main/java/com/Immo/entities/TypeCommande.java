package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SuppressWarnings("serial")
public class TypeCommande implements  Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idTypeCommande;
	private String libelleTypeCommande;
	public TypeCommande() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TypeCommande(int idTypeCommande, String libelleTypeCommande) {
		super();
		this.idTypeCommande = idTypeCommande;
		this.libelleTypeCommande = libelleTypeCommande;
	}
	public int getIdTypeCommande() {
		return idTypeCommande;
	}
	public void setIdTypeCommande(int idTypeCommande) {
		this.idTypeCommande = idTypeCommande;
	}
	public String getLibelleTypeCommande() {
		return libelleTypeCommande;
	}
	public void setLibelleTypeCommande(String libelleTypeCommande) {
		this.libelleTypeCommande = libelleTypeCommande;
	}
	@Override
	public String toString() {
		return "TypeCommande [idTypeCommande=" + idTypeCommande + ", libelleTypeCommande=" + libelleTypeCommande + "]";
	}

	
}
