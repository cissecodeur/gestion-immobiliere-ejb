package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SuppressWarnings("serial")
public class Profil implements Serializable{
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idProfil;
	private String libelleProfil;
	
	public Profil() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Profil(int idProfil, String libelleProfil) {
		super();
		this.idProfil = idProfil;
		this.libelleProfil = libelleProfil;
	}

	public int getIdProfil() {
		return idProfil;
	}

	public void setIdProfil(int idProfil) {
		this.idProfil = idProfil;
	}

	public String getLibelleProfil() {
		return libelleProfil;
	}

	public void setLibelleProfil(String libelleProfil) {
		this.libelleProfil = libelleProfil;
	}

	@Override
	public String toString() {
		return "Profil [idProfil=" + idProfil + ", libelleProfil=" + libelleProfil + "]";
	}
	
	
	

}
