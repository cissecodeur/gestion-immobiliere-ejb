package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
public class Compte implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int    idCompte;
	private String nomCompte;
	private String addresseCompte;
	private String telephoneCompte;
	private int    etatCompte;
	
	@ManyToOne
	@JoinColumn(name="idClient",referencedColumnName="idClient")
	private Client client;

	public Compte() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Compte(int idCompte, String nomCompte, String addresseCompte, String telephoneCompte, int etatCompte,
			Client client) {
		super();
		this.idCompte = idCompte;
		this.nomCompte = nomCompte;
		this.addresseCompte = addresseCompte;
		this.telephoneCompte = telephoneCompte;
		this.etatCompte = etatCompte;
		this.client = client;
	}

	public int getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(int idCompte) {
		this.idCompte = idCompte;
	}

	public String getNomCompte() {
		return nomCompte;
	}

	public void setNomCompte(String nomCompte) {
		this.nomCompte = nomCompte;
	}

	public String getAddresseCompte() {
		return addresseCompte;
	}

	public void setAddresseCompte(String addresseCompte) {
		this.addresseCompte = addresseCompte;
	}

	public String getTelephoneCompte() {
		return telephoneCompte;
	}

	public void setTelephoneCompte(String telephoneCompte) {
		this.telephoneCompte = telephoneCompte;
	}

	public int getEtatCompte() {
		return etatCompte;
	}

	public void setEtatCompte(int etatCompte) {
		this.etatCompte = etatCompte;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return "Compte [idCompte=" + idCompte + ", nomCompte=" + nomCompte + ", addresseCompte=" + addresseCompte
				+ ", telephoneCompte=" + telephoneCompte + ", etatCompte=" + etatCompte + ", client=" + client + "]";
	}
	
	
}
