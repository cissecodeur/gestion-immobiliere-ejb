package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
public class Quartier implements Serializable{
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idQuartier;
	private String nomQuartier;
	
	@ManyToOne
	@JoinColumn(name="idCommune",referencedColumnName="idCommune")
	private Commune commune;

	public Quartier() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Quartier(int idQuartier, String nomQuartier, Commune commune) {
		super();
		this.idQuartier = idQuartier;
		this.nomQuartier = nomQuartier;
		this.commune = commune;
	}

	public int getIdQuartier() {
		return idQuartier;
	}

	public void setIdQuartier(int idQuartier) {
		this.idQuartier = idQuartier;
	}

	public String getNomQuartier() {
		return nomQuartier;
	}

	public void setNomQuartier(String nomQuartier) {
		this.nomQuartier = nomQuartier;
	}

	public Commune getCommune() {
		return commune;
	}

	public void setCommune(Commune commune) {
		this.commune = commune;
	}

	@Override
	public String toString() {
		return "Quartier [idQuartier=" + idQuartier + ", nomQuartier=" + nomQuartier + ", commune=" + commune + "]";
	}
	
	

}
