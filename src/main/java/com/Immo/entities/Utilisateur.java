package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
public class Utilisateur implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idUtilisateur;
	private String nomUtilisateur;
	private String prenomUtilisateur;
	private String emailUtilisateur;
	private String telephoneUtilisateur;
	private String fonctionUtlisateur;
	private String passwordUtilisateur;
	private int    etatUtilisateur;
	
	// Methode d'ajout des classes etrangeres
	
	@ManyToOne
	@JoinColumn(name="idCompte",referencedColumnName="idCompte")
	private Compte compte;
	
	@ManyToOne
	@JoinColumn(name="idProfil",referencedColumnName="idProfil")
	private Profil profil;
	
	@ManyToOne
	@JoinColumn(name="idGroupe",referencedColumnName="idGroupe")
	private Groupe groupe;

	public Utilisateur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Utilisateur(int idUtilisateur, String nomUtilisateur, String prenomUtilisateur, String emailUtilisateur,
			String telephoneUtilisateur, String fonctionUtlisateur, String passwordUtilisateur, int etatUtilisateur,
			Compte compte, Profil profil, Groupe groupe) {
		super();
		this.idUtilisateur = idUtilisateur;
		this.nomUtilisateur = nomUtilisateur;
		this.prenomUtilisateur = prenomUtilisateur;
		this.emailUtilisateur = emailUtilisateur;
		this.telephoneUtilisateur = telephoneUtilisateur;
		this.fonctionUtlisateur = fonctionUtlisateur;
		this.passwordUtilisateur = passwordUtilisateur;
		this.etatUtilisateur = etatUtilisateur;
		this.compte = compte;
		this.profil = profil;
		this.groupe = groupe;
	}

	public int getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public String getNomUtilisateur() {
		return nomUtilisateur;
	}

	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}

	public String getPrenomUtilisateur() {
		return prenomUtilisateur;
	}

	public void setPrenomUtilisateur(String prenomUtilisateur) {
		this.prenomUtilisateur = prenomUtilisateur;
	}

	public String getEmailUtilisateur() {
		return emailUtilisateur;
	}

	public void setEmailUtilisateur(String emailUtilisateur) {
		this.emailUtilisateur = emailUtilisateur;
	}

	public String getTelephoneUtilisateur() {
		return telephoneUtilisateur;
	}

	public void setTelephoneUtilisateur(String telephoneUtilisateur) {
		this.telephoneUtilisateur = telephoneUtilisateur;
	}

	public String getFonctionUtlisateur() {
		return fonctionUtlisateur;
	}

	public void setFonctionUtlisateur(String fonctionUtlisateur) {
		this.fonctionUtlisateur = fonctionUtlisateur;
	}

	public String getPasswordUtilisateur() {
		return passwordUtilisateur;
	}

	public void setPasswordUtilisateur(String passwordUtilisateur) {
		this.passwordUtilisateur = passwordUtilisateur;
	}

	public int getEtatUtilisateur() {
		return etatUtilisateur;
	}

	public void setEtatUtilisateur(int etatUtilisateur) {
		this.etatUtilisateur = etatUtilisateur;
	}

	public Compte getCompte() {
		return compte;
	}

	public void setCompte(Compte compte) {
		this.compte = compte;
	}

	public Profil getProfil() {
		return profil;
	}

	public void setProfil(Profil profil) {
		this.profil = profil;
	}

	public Groupe getGroupe() {
		return groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

	@Override
	public String toString() {
		return "Utilisateur [idUtilisateur=" + idUtilisateur + ", nomUtilisateur=" + nomUtilisateur
				+ ", prenomUtilisateur=" + prenomUtilisateur + ", emailUtilisateur=" + emailUtilisateur
				+ ", telephoneUtilisateur=" + telephoneUtilisateur + ", fonctionUtlisateur=" + fonctionUtlisateur
				+ ", passwordUtilisateur=" + passwordUtilisateur + ", etatUtilisateur=" + etatUtilisateur + ", compte="
				+ compte + ", profil=" + profil + ", groupe=" + groupe + "]";
	}
	
	
}
