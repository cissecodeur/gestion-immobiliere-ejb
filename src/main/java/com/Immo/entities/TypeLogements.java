package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SuppressWarnings("serial")
public class TypeLogements implements Serializable{
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idTypeLogement;
	private String libelleTypeLogement;
	
	public TypeLogements(int idTypeLogement, String libelleTypeLogement) {
		super();
		this.idTypeLogement = idTypeLogement;
		this.libelleTypeLogement = libelleTypeLogement;
	}

	public TypeLogements() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdTypeLogement() {
		return idTypeLogement;
	}

	public void setIdTypeLogement(int idTypeLogement) {
		this.idTypeLogement = idTypeLogement;
	}

	public String getLibelleTypeLogement() {
		return libelleTypeLogement;
	}

	public void setLibelleTypeLogement(String libelleTypeLogement) {
		this.libelleTypeLogement = libelleTypeLogement;
	}

	@Override
	public String toString() {
		return "TypeLogements [idTypeLogement=" + idTypeLogement + ", libelleTypeLogement=" + libelleTypeLogement + "]";
	}
	
	

}
