package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SuppressWarnings("serial")
public class TypeClient implements Serializable{
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idTypeClient;
	private String libelleClient;
	
	public TypeClient() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TypeClient(int idTypeClient, String libelleClient) {
		super();
		this.idTypeClient = idTypeClient;
		this.libelleClient = libelleClient;
	}

	public int getIdTypeClient() {
		return idTypeClient;
	}

	public void setIdTypeClient(int idTypeClient) {
		this.idTypeClient = idTypeClient;
	}

	public String getLibelleClient() {
		return libelleClient;
	}

	public void setLibelleClient(String libelleClient) {
		this.libelleClient = libelleClient;
	}

	@Override
	public String toString() {
		return "TypeClient [idTypeClient=" + idTypeClient + ", libelleClient=" + libelleClient + "]";
	}
	
	

}
