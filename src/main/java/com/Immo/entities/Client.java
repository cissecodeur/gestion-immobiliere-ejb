package com.Immo.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class Client implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int    idClient;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int    idClient;
	private String nomClient;
	private String prenomClient;
	private String contactClient;
	private String activiteClient;

	@ManyToOne
    @JoinColumn(name="idTypeClient",referencedColumnName="idTypeClient")
	private TypeClient typeclient;

	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Client(int idClient, String nomClient, String prenomClient, String contactClient, String activiteClient,
			TypeClient typeclient) {
		super();
		this.idClient = idClient;
		this.nomClient = nomClient;
		this.prenomClient = prenomClient;
		this.contactClient = contactClient;
		this.activiteClient = activiteClient;
		this.typeclient = typeclient;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getPrenomClient() {
		return prenomClient;
	}

	public void setPrenomClient(String prenomClient) {
		this.prenomClient = prenomClient;
	}

	public String getContactClient() {
		return contactClient;
	}

	public void setContactClient(String contactClient) {
		this.contactClient = contactClient;
	}

	public String getActiviteClient() {
		return activiteClient;
	}

	public void setActiviteClient(String activiteClient) {
		this.activiteClient = activiteClient;
	}

	public TypeClient getTypeclient() {
		return typeclient;
	}

	public void setTypeclient(TypeClient typeclient) {
		this.typeclient = typeclient;
	}

	@Override
	public String toString() {
		return "Client [idClient=" + idClient + ", nomClient=" + nomClient + ", prenomClient=" + prenomClient
				+ ", contactClient=" + contactClient + ", activiteClient=" + activiteClient + ", typeclient="
				+ typeclient + "]";
	}
	
	
	
	
	
	
}
